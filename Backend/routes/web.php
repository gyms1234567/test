<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


use Firebase\JWT\JWT ;  

// Login API (for both Admin & User)
$router->post('/login', function(Illuminate\Http\Request $request) {
	
	$username = $request->input("username");
	$password = $request->input("password");
	
	$result = app('db')->select("SELECT UserID, password, IsAdmin FROM running_user WHERE Username=?",
									[$username]);
									
	$loginResult = new stdClass();
	
	if(count ($result) == 0) {
			$loginResult->status = "fail";
			$loginResult->reason = "User is not founded";
	}else {
	
		if(app('hash')->check($password, $result[0]->password)){
			$loginResult->status = "success";
			
			$payload = [
				'iss' => "running_system",
				'sub' => $result[0]->UserID,
				'iat' => time(),
				'exp' => time()+ 30 * 60 * 60,
			
			];
			
			$loginResult->token = JWT::encode($payload, env('APP_KEY'));
			$loginResult->isAdmin = $result[0]->IsAdmin;
			
		}else {
			$loginResult->status = "fail";
			$loginResult->reason = "Incorrect Password";
		}
	}
	return response()->json($loginResult);
});
 
//Public API (Can be called without API Token
$router->get('/list_all_event', function () {
	
	$results = app('db')->select("SELECT * FROM running_event");
	return response()->json($results);
});

$router->post('/register_user', function(Illuminate\Http\Request $request) {
	
	$name = $request->input("name");
	$surname = $request->input("surname");
	$email = $request->input("email");
	$username = $request->input("username");
	$password = app('hash')->make($request->input("password"));
	
	$query = app('db')->insert('INSERT into running_user
					(Username, Password, Name, Surname, Email, IsAdmin)
					VALUE (?, ?, ?, ?, ?, ?)',
					[ $username,
					  $password,
					  $name,
					  $surname,
					  $email,
					  'n'] );
	return "Ok";
	
});


// User's Backend API  (Must be called with API_Token

$router->get('/get_user_profile', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {

	$user = app('auth')->user();
	$user_id = $user->id;
	
	$results = app('db')->select('SELECT * FROM running_user
							      WHERE (running_user.UserID=?)',
										[$user_id]);
	return response()->json($results);
}]);

$router->get('/list_user_event', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	
	$user = app('auth')->user();
	$user_id = $user->id;
	
	$results = app('db')->select('SELECT EventName, EventPlace, EventDate, Confirm
								  FROM running_event, running_registration, running_user
							      WHERE (running_registration.EventID=running_event.EventID)
										AND (running_user.UserID=?)
										AND  (running_user.UserID=running_registration.UserID)',
										[$user_id]);
	return response()->json($results);
}]);

$router->post('/register_event', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	
	$event_id = $request->input("event_id");
	
	$user = app('auth')->user();
	$user_id = $user->id;
	
	$query = app('db')->insert('INSERT into running_registration
					(EventID, UserID, Confirm)
					VALUE (?, ?, ?)',
					[ $event_id,
					  $user_id,
					  'n'] );
	return "Ok";
	
}]);


// Admin Backend API
$router->post('/admin_add_event', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	
	$user = app('auth')->user();
	$user_id = $user->id;
	
	//Check if user is an admin
	$results = app('db')->select("SELECT IsAdmin FROM running_user WHERE UserID=?",
								  [$user_id]);
	
	if ($results[0]->IsAdmin == 'n') {
		return "Permission Denied";
	}else {
		
		$event_name = $request->input("event_name");	
		$event_datetime = strtotime($request->input("event_datetime"));
		$event_datetime = date('Y-m-d H:i:s', $event_datetime);
		
		$event_place = $request->input("event_place");
		
		$query = app('db')->insert('INSERT into running_event
						(EventName, EventDate, EventPlace)
						VALUE (?, ?, ?)',
						[ $event_name,
						  $event_datetime,
						  $event_place ] );
		return "Ok";
	}

}]);

$router->put('/admin_confirm_user', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	
	$user = app('auth')->user();
	$user_id = $user->id;
	
	//Check if user is an admin
	$results = app('db')->select("SELECT IsAdmin FROM running_user WHERE UserID=?",
								  [$user_id]);
	
	if ($results[0]->IsAdmin == 'n') {
		return "Permission Denied";
	}else {
		$reg_id = $request->input("reg_id");
		$query = app('db')->update('UPDATE running_registration
						SET Confirm=?
						WHERE RegID=?',
						[ 'y',
						  $reg_id] );
		
		return "Ok";	
	}
}]);


$router->get('/admin_list_register', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	
	$user = app('auth')->user();
	$user_id = $user->id;
	
	//Check if user is an admin
	$results = app('db')->select("SELECT IsAdmin FROM running_user WHERE UserID=?",
								  [$user_id]);
	
	if ($results[0]->IsAdmin == 'n') {
		return "Permission Denied";
	}else {
		
			$results = app('db')->select('SELECT RegID, Name, Surname, EventName, Confirm
												FROM running_event, running_registration, running_user
												WHERE (running_registration.EventID=running_event.EventID)
												AND (running_registration.Confirm=?)
												AND  (running_user.UserID=running_registration.UserID)',
												['n']);
		
		return response()->json($results);
	}
	
}]);



