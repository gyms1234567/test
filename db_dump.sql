-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for running_system
CREATE DATABASE IF NOT EXISTS `running_system` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `running_system`;

-- Dumping structure for table running_system.running_event
CREATE TABLE IF NOT EXISTS `running_event` (
  `EventID` int(11) NOT NULL AUTO_INCREMENT,
  `EventName` varchar(50) NOT NULL,
  `EventDate` datetime NOT NULL,
  `EventPlace` varchar(50) NOT NULL,
  PRIMARY KEY (`EventID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table running_system.running_event: ~4 rows (approximately)
/*!40000 ALTER TABLE `running_event` DISABLE KEYS */;
REPLACE INTO `running_event` (`EventID`, `EventName`, `EventDate`, `EventPlace`) VALUES
	(1, 'วิ่งลิมรัก', '2002-03-11 06:21:17', 'คอหงส์'),
	(2, 'วิ่งเพื่อน้อง', '2021-03-11 22:21:51', 'หาดใหญ่'),
	(3, 'วิ่งอีกแล้ว', '2022-12-18 06:00:00', 'เขาคอหงส์'),
	(4, 'วิ่งไปเล้ย', '1970-01-01 00:00:00', 'ยะม้า');
/*!40000 ALTER TABLE `running_event` ENABLE KEYS */;

-- Dumping structure for table running_system.running_registration
CREATE TABLE IF NOT EXISTS `running_registration` (
  `RegID` int(11) NOT NULL AUTO_INCREMENT,
  `EventID` int(11) DEFAULT '0',
  `UserID` int(11) DEFAULT '0',
  `Confirm` char(50) DEFAULT 'n',
  PRIMARY KEY (`RegID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table running_system.running_registration: ~3 rows (approximately)
/*!40000 ALTER TABLE `running_registration` DISABLE KEYS */;
REPLACE INTO `running_registration` (`RegID`, `EventID`, `UserID`, `Confirm`) VALUES
	(1, 1, 1, 'y'),
	(2, 2, 1, 'n'),
	(3, 3, 1, 'y');
/*!40000 ALTER TABLE `running_registration` ENABLE KEYS */;

-- Dumping structure for table running_system.running_user
CREATE TABLE IF NOT EXISTS `running_user` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(50) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Surname` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `IsAdmin` char(50) DEFAULT 'n',
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table running_system.running_user: ~2 rows (approximately)
/*!40000 ALTER TABLE `running_user` DISABLE KEYS */;
REPLACE INTO `running_user` (`UserID`, `Username`, `Password`, `Name`, `Surname`, `Email`, `IsAdmin`) VALUES
	(1, 'chinnapong', '$2y$10$rxu0aOF2kvpUcq4RR.JF4eaKSe34/IBvvvLIl5vN6gfxaeDEYhfl.', 'ชินพงศ์', 'อังสุโชติเมธี', 'chinnapong.a@gmail.com', 'n'),
	(2, 'admin', '$2y$10$8gcDuQoDfGXdBpo2Ht3mLeQ60cxmBvAieXvK0oK03j1ev1W3xrKSG', 'ชินพงศ์', 'อังสุโชติเมธี', 'chinnapong.a@gmail.com', 'y');
/*!40000 ALTER TABLE `running_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
